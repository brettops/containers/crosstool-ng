# This Dockerfile is intended for crosstool-ng v1.21.0 and up
ARG CONTAINER_OS="ubuntu:22.04"
ARG CONTAINER_PROXY
# hadolint ignore=DL3006
FROM ${CONTAINER_PROXY}${CONTAINER_OS}

ENV DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        autoconf \
        automake \
        bash \
        bash-completion \
        bison \
        build-essential \
        ca-certificates \
        curl \
        file \
        flex \
        gawk \
        gcc \
        gnupg \
        help2man \
        libncurses-dev \
        libtool \
        libtool-bin \
        tar \
        texinfo \
        unzip \
        vim \
        xz-utils \
    && rm -rf /var/lib/apt/lists/*

SHELL [ "/bin/bash", "-euo", "pipefail", "-c" ]

ARG CROSSTOOL_NG_VERSION="1.25.0"
ENV CROSSTOOL_NG_VERSION="$CROSSTOOL_NG_VERSION"

RUN curl -sSOL "https://github.com/crosstool-ng/crosstool-ng/releases/download/crosstool-ng-${CROSSTOOL_NG_VERSION}/crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz" \
    && curl -sSOL "https://github.com/crosstool-ng/crosstool-ng/releases/download/crosstool-ng-${CROSSTOOL_NG_VERSION}/crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz.sha512" \
    && curl -sSOL "https://github.com/crosstool-ng/crosstool-ng/releases/download/crosstool-ng-${CROSSTOOL_NG_VERSION}/crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz.sig" \
    && sha512sum --check crosstool-ng-1.25.0.tar.xz.sha512 \
    && gpg --keyserver pgp.surfnet.nl --recv-keys "35B871D1" "11D618A4" "1F30EF2E" \
    && gpg --verify "crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz.sig" \
    && tar xf "crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz" \
    && rm -f \
        "crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz" \
        "crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz.sha512" \
        "crosstool-ng-${CROSSTOOL_NG_VERSION}.tar.xz.sig" \
    && pushd "crosstool-ng-${CROSSTOOL_NG_VERSION}" \
    && ./bootstrap \
    && ./configure --prefix="/usr/local/crosstool-ng" \
    && make -j \
    && make install \
    && mkdir /etc/bash_completion.d \
    && cp -f ./bash-completion/ct-ng /etc/bash_completion.d/ct-ng \
    && echo ". /etc/bash_completion" >> /etc/bash.bashrc \
    && popd \
    && rm -rf "crosstool-ng-${CROSSTOOL_NG_VERSION}"

ENV PATH="$PATH:/usr/local/crosstool-ng/bin"

RUN useradd -m -U crosstool

USER crosstool
