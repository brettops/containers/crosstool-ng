# crosstool-NG container

<!-- BADGIE TIME -->

[![brettops container](https://img.shields.io/badge/brettops-container-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/containers/crosstool-ng?branch=main)](https://gitlab.com/brettops/containers/crosstool-ng/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Build a cross compiler with the `ct-ng` CLI.

## Build & Run

A local path must be mounted inside the container to retrieve the resulting
toolchain. Crosstool must be run as non-root user, so the image will not
be writable.

With `make`:

```
make
```

With `docker` commands:

```
docker build -t crosstool .
docker run --rm -it -v $(pwd):/code -w /code crosstool
```

Once inside, you will be able to use the `ct-ng` tool:

```
# ct-ng list-samples
# ct-ng mips-unknown-elf
# ct-ng menuconfig
# ct-ng build
```
